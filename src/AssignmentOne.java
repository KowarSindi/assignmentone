import java.util.Scanner;

import javax.swing.JOptionPane;

public class AssignmentOne 
{
	public static void main(String [] args)
	{
		
		System.out.println("Enter a degree in fahrenheit.");
		Scanner keyboard = new Scanner (System.in);
		int n1;
		n1 = keyboard.nextInt();
		System.out.println("The value entered will be converted to celsius.");
		System.out.println(n1 + " degrees in fahrenheit is equal to ");
		System.out.println(5*(n1-32)/9);
		System.out.println("degrees in celsius");
		JOptionPane.showMessageDialog(null, "Thank you!");
		keyboard.close();
	}
}